import { dummyData } from "../util";
const { REACT_APP_NEWSAPI_KEY1, REACT_APP_NEWSAPI_KEY2 } = process.env;
// export const fetchNews = (country, page,size) => {
//   return new Promise((resolve, reject) => {
//     fetch(
//       "https://newsapi.org/v2/top-headlines?apiKey=" +
//       REACT_APP_NEWSAPI_KEY1 +
//         `&country=${country}` +
//         `&pageSize=${size}` +
//         `&page=${page}`
//     )
//       .then((res) => res.json())
//       .then((data) => resolve(data))
//       .catch((error) => console.log(error));
//   });
// };
export const fetchNews = (country, page) => {
  return new Promise((resolve, reject) => {    
    if (page == 1) {
      resolve({
        status: "ok",
        totalResults: 20,
        articles: dummyData.articles.slice(0, 10),
      });
    } else {
      resolve({
        status: "ok",
        totalResults: 20,
        articles: dummyData.articles.slice(10),
      });
    }
  });
};
