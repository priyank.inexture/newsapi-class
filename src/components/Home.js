import React from "react";
import { Link } from "react-router-dom";
const links = [  
  {
    text: "(Class component) Lazy loading Component infinite scroll",
    to: "/class/lazyscroll",
  },
  {
    text: "(Class component) Lazy loading Component pagination",
    to: "/class/lazypagination",
  },
];
function Home() {
  return (
    <ul>
      {links.map((l,i) => (
        <li key={i}>
          <Link to={l.to}>{l.text}</Link>
        </li>
      ))}
    </ul>
  );
}

export default Home;
