import React, { Component } from "react";
import { Pagination, Select, Layout } from "antd";
import "antd/dist/antd.css";
//custom components
import Article from "./Article";
import Loader from "../Loader";
import ErrorComponent from "../ErrorComponent";
//api calls
import { fetchNews } from "../../api/Article";

//static data
import { countries } from "../../util";
const { Option } = Select;
const { Content, Header } = Layout;
export class ArticleList extends Component {
  constructor() {
    super();
    this.lastEl = React.createRef(null);

    this.state = {
      articles: [],
      loading: true,
      hasError: false,
      country: localStorage.getItem("prefered_country")
        ? localStorage.getItem("prefered_country")
        : "us",
      //   pageSize: 30,
      currentPage: 1,
      totalArticle: 0,
    };
  }
  componentDidMount = () => {
    document.title = "Top Headlines | " + countries[this.state.country];
    this.handleArticles();
  };
  componentDidUpdate = () => {
    if(!this.state.loading){
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    }
  };
  componentWillUnmount = () => {
    document.title = "Newsapi App";
  };
  handleArticles = () => {
    fetchNews(this.state.country, this.state.currentPage).then((data) => {
      if (data.status === "ok") {
        // let article = this.state.articles.concat(data.articles);
        this.setState({
          articles: data.articles,
          loading: false,
          totalArticle: data.totalResults,
        });
      } else {
        this.setState({ hasError: true });
      }
    });
  };
  onChangePagination = (page) => {
    this.setState({ loading: true });
    localStorage.setItem("currentPage", page);
    this.setState({ currentPage: page }, () => this.handleArticles());
  };
  onChangeCountry = (country) => {
    localStorage.setItem("prefered_country", country);
    this.setState({ loading: true });
    this.setState({ country: country, currentPage: 1 }, () =>
      this.handleArticles()
    );
    document.title = "Top Headlines | " + countries[country];
  };
  render() {
    return (
      <>
        <Header
          style={{ position: "sticky", zIndex: 1, width: "100%", top: 0 }}
        >
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Select Country"
            optionFilterProp="children"
            defaultValue={this.state.country}
            onChange={this.onChangeCountry}
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {Object.entries(countries).map(([key, name]) => (
              <Option value={key} key={key}>
                {name}
              </Option>
            ))}
          </Select>
        </Header>
        <Content>
          {this.state.loading && !this.state.hasError ? (
            <Loader />
          ) : !this.state.hasError ? (
            <>
              <div className="article__container">
                {this.state.articles.map((articleProps, index) => (
                  <Article
                    key={index}
                    {...articleProps}
                    onDetailPageClick={() => {
                      this.props.onDetailPageClick(articleProps);
                    }}
                  />
                ))}
                <div ref={this.lastEl} className="last_elment"></div>
              </div>
              <div className="center_pagination">
                <Pagination
                  onChange={this.onChangePagination}
                  defaultCurrent={this.state.currentPage}
                  current={this.state.currentPage}
                  total={this.state.totalArticle}
                  pageSize={10}
                />
              </div>
            </>
          ) : (
            <ErrorComponent />
          )}
        </Content>
      </>
    );
  }
}

export default ArticleList;
