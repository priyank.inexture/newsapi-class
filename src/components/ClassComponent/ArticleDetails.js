import React, { Component } from "react";
import { Layout, Tag } from "antd";
import "../../css/detail.css";
import { formateDate } from "../../util";
const { Content, Header } = Layout;
let prop = {
  source: {
    id: "cbs-news",
    name: "CBS News",
  },
  author: "Stephen Gandel",
  title:
    "GameStop shares plunge below $100 as WallStreetBets traders face huge losses - CBS News",
  description:
    "SEC has said it's investigating whether brokers acted responsibly and whether the video game retailer's shares were manipulated.",
  url: "https://www.cbsnews.com/news/gamestop-stock-down-gme-losses/",
  urlToImage:
    "https://cbsnews2.cbsistatic.com/hub/i/r/2021/01/30/36fb9d54-c150-4fb8-9c33-0a4374c154ca/thumbnail/1200x630/687fbe2420f49341f16a955b2994451a/cbsn-fusion-stock-market-slips-as-gamestop-shares-soar-thumbnail-636354-640x360.jpg",
  publishedAt: "2021-02-03T12:02:00Z",
  content:
    "Californias fight over reopening schools amid the coronavirus pandemic took a surprising turn Wednesday, when the city of San Francisco became the first in the state, possibly the country, to sue the… [+4923 chars]",
};
export class ArticleDetails extends Component {
  componentDidMount() {    
    document.title = this.props.article.title;
  }
  componentWillUnmount() {
    document.title = "Newsapi App";
  }
  componentDidCatch() {
    console.log("error");
  }
  render() {
    return (
      <>
        <Header />
        <Content>
          <div className="detail_article_container">
            <div className="detail_article_image">
              <img src={this.props.article.urlToImage} alt="" />
            </div>
            <div className="detail_article_content">
              <h1>{this.props.article.title}</h1>
              <h2>{this.props.article.description}</h2>
              <Tag color="#108ee9">
                <a href={this.props.article.url}>
                  {this.props.article.source.name
                    ? this.props.article.source.name
                    : "source"}
                </a>
              </Tag>
              <time>{formateDate(this.props.article.publishedAt)} </time>
              {this.props.article.author && this.props.article.author != "" && (
                <>
                  | <b>{this.props.article.author}</b>
                </>
              )}
              <p>{this.props.article.content}</p>
            </div>
          </div>
        </Content>
      </>
    );
  }
}

export default ArticleDetails;
