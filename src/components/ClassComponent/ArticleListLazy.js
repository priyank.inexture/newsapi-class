import React, { Component } from "react";
import { Select, Layout } from "antd";
//custom components
import Article from "./Article";
import Loader from "../Loader";
import ErrorComponent from "../ErrorComponent";
//api calls
import { fetchNews } from "../../api/Article";

//static data
import { countries } from "../../util";
const { Option } = Select;
const { Content, Header } = Layout;
export class ArticleListLazy extends Component {
  constructor() {
    super();
    this.lastEl = React.createRef(null);
    this.state = {
      articles: [],
      loading: true,
      pageSize: 10,
      hasError: false,
      country: localStorage.getItem("prefered_country")
        ? localStorage.getItem("prefered_country")
        : "us",
      //   pageSize: 30,
      currentPage: 1,
      totalArticle: 0,
    };
    this.observer = new IntersectionObserver(
      (entries) => {
        let last = entries[0];
        if (last.isIntersecting) {
          console.log("visible");
          if (
            this.state.pageSize * this.state.currentPage <
            this.state.totalArticle
          )
            this.onChangePagination(this.state.currentPage + 1);
          else {
            alert("over");
            this.observer.unobserve(this.lastEl.current);
          }
          // this.setState({currentPage:2})
        }
      },
      { threshold: 1 }
    );
  }
  componentDidMount = () => {
    document.title = "Top Headlines | " + countries[this.state.country];
    if (this.props.article.length > 0) {
      this.setState({
        articles: this.props.article,
        loading:false
      });
    }else
    this.handleArticles();
  };
  componentDidUpdate = () => {
    if (this.lastEl.current) {      
      this.observer.observe(this.lastEl.current);
    }
  };
  componentWillUnmount = () => {
    this.observer.unobserve(this.lastEl.current);
  };
  componentWillUnmount = () => {
    document.title = "Newsapi App";
  };
  handleArticles = () => {
    fetchNews(
      this.state.country,
      this.state.currentPage,
      this.state.pageSize
    ).then((data) => {
      if (data.status === "ok") {
        let article = this.state.articles.concat(data.articles);
        // this.setState({});
        this.setState({
          articles: article,
          loading: false,
          totalArticle: data.totalResults,
        });
        this.props.storePrevState(article, this.state.currentPage);
      } else {
        this.setState({ hasError: true });
      }
    });
  };
  onChangePagination = (page) => {
    // this.setState({ loading: true });
    localStorage.setItem("currentPage", page);
    this.setState({ currentPage: page }, () => this.handleArticles());
  };
  onChangeCountry = (country) => {
    localStorage.setItem("prefered_country", country);
    this.setState({ loading: true });
    this.setState({ country: country, currentPage: 1 }, () =>
      this.handleArticles()
    );
    document.title = "Top Headlines | " + countries[country];
  };
  render() {
    return (
      <>
        <Header
          style={{ position: "sticky", zIndex: 1, width: "100%", top: 0 }}
        >
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Select Country"
            optionFilterProp="children"
            defaultValue={this.state.country}
            onChange={this.onChangeCountry}
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {Object.entries(countries).map(([key, name]) => (
              <Option value={key} key={key}>
                {name}
              </Option>
            ))}
          </Select>
        </Header>
        <Content>
          {this.state.loading && !this.state.hasError ? (
            <Loader />
          ) : !this.state.hasError ? (
            <>
              <div className="article__container">
                {this.state.articles.map((articleProps, index) => (
                  <Article
                    key={index}
                    {...articleProps}
                    onDetailPageClick={() => {
                      this.props.onDetailPageClick(articleProps);
                    }}
                  />
                ))}
                <div ref={this.lastEl} className="last_elment"></div>
              </div>
              {/* <div className="center_pagination">
                <Pagination
                  onChange={this.onChangePagination}
                  defaultCurrent={this.state.currentPage}
                  current={this.state.currentPage}
                  total={this.state.totalArticle}
                  pageSize={this.state.pageSize}
                />
              </div> */}
            </>
          ) : (
            <ErrorComponent />
          )}
        </Content>
      </>
    );
  }
}

export default ArticleListLazy;
