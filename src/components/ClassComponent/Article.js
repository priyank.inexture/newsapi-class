import React, { Component } from "react";
import { Card } from "antd";
import { Link } from "react-router-dom";
import {formateDate} from '../../util'
export class Article extends Component {  
  render() {
    return (
      <Link to="/detail" onClick={this.props.onDetailPageClick}>
        <Card hoverable className="article__card">
          <div className="article__image">
            <img src={this.props.urlToImage} alt="" />
          </div>
          <h1 className="article__heading">{this.props.title}</h1>
          <p>{this.props.description}</p>
          <p className="secondary__info">
            {formateDate(this.props.publishedAt)}{" "}
            {this.props.author && `| ${this.props.author}`}
          </p>
        </Card>
      </Link>
    );
  }
}

export default Article;
