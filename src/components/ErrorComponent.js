import React, { Component } from 'react'

export class ErrorComponent extends Component {
    render() {
        return (
            <div className="error__component">
                Some Error has occured
            </div>
        )
    }
}

export default ErrorComponent
