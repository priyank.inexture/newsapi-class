import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Layout } from "antd";
import ArticleListClass from "./components/ClassComponent/ArticleList";
import ArticleDetailsClass from "./components/ClassComponent/ArticleDetails";
import ArticleListLazyClass from "./components/ClassComponent/ArticleListLazy";
import Home from "./components/Home";
import "antd/dist/antd.css";
export class App extends Component {
  constructor() {
    super();
    this.state = {
      prevArticles: [],
      prevPage: 0,
      selectedArticle: {
        source: { id: "", name: "" },
        author: "",
        title: "",
        description: "",
        url: "",
        urlToImage: "",
        publishedAt: "",
        content: "",
      },
    };
  }
  componentDidMount() {
    document.title = "Newsapi App";
  }
  onDetailPageClick = (article) => {
    this.setState({ selectedArticle: article });
  };
  storePrevState = (articles, page) => {
    this.setState({ prevArticles: articles, prevPage: page });
  };
  render() {
    return (
      <Layout className="layout">
        <Router>
          <Switch>
            <Route path="/detail">
              <ArticleDetailsClass article={this.state.selectedArticle} />
            </Route>
           
            <Route path="/class/lazyscroll">
              <ArticleListLazyClass
                article={this.state.prevArticles}
                page={this.state.prevPage}
                onDetailPageClick={this.onDetailPageClick}
                storePrevState={this.storePrevState}
              />
            </Route>
            <Route path="/class/lazypagination">
              <ArticleListClass onDetailPageClick={this.onDetailPageClick} />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
      </Layout>
    );
  }
}

export default App;
